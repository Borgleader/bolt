#include <bolt/lexer/token.hpp>

static std::string tokenTypeNames[] = {
    "Whitespace",

    "var",
    "if",
    "else",
    "for",
    "while",
    "return",

    "bool",
    "int8",
    "int16",
    "int32",
    "int64",
    "string",
    "uint8",
    "uint16",
    "uint32",
    "uint64",
    "void",

    "Identifier",

    "Decimal Literal",
    "Binary Literal",
    "Octal Literal",
    "Hexadecimal Literal",

    "String Literal",

    "+",
    "-",
    "*",
    "/",
    "<=",
    "<",
    ">=",
    ">",
    "==",
    "!=",
    "=",

    "(",
    ")",
    "{",
    "}",

    ";",
    ","
};

namespace std
{
    std::string to_string(bolt::TokenType t)
    {
        assert(t < bolt::NumTokenTypes);
        return tokenTypeNames[t - boost::spirit::lex::min_token_id];
    }

    std::string to_string(bolt::TokenIterRange r)
    {
        return std::string(r.begin(), r.end());
    }
    
    std::string to_string(const bolt::Token& t)
    {
        return std::string(t.value.begin(), t.value.end());
    }
}
