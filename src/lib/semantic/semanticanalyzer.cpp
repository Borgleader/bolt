#include <exception>
#include <limits>

#include <boost/multiprecision/cpp_int.hpp>

#include <bolt/symboltable.hpp>
#include <bolt/ast/moduleast.hpp>
#include <bolt/semantic/semanticanalyzer.hpp>
#include <bolt/utils/utils.hpp>

namespace mp = boost::multiprecision;

namespace bolt
{
    namespace visitor
    {
        struct IntegralConstantOverflowException : public std::exception
        {
            IntegralConstantOverflowException(std::string why) : why(why)
            {
            }

            const char* what() const throw() override { return why.c_str(); }

        private:
            std::string why;
        };

        struct UndefinedFunctionException : public std::exception
        {
            UndefinedFunctionException(std::string why) : why(why)
            {
            }

            const char* what() const throw() override { return why.c_str(); }

        private:
            std::string why;
        };

        namespace detail
        {
            template <typename T>
            struct IsSameExprType : public boost::static_visitor<bool>
            {
                bool operator()(SymbolTable::Function& f) const
                {
                    return IsSameType(f);
                }

                bool operator()(SymbolTable::Variable& v) const
                {
                    return IsSameType(v);
                }

            private:
                template <typename V>
                bool IsSameType(V&) const
                {
                    return std::is_same<T, V>::value;
                }
            };
        }
        
        namespace expr
        {
            class Analyze : public boost::static_visitor<>
            {
            public:
                Analyze(ScopeView& table) : symbolTable(table)
                {
                }

                void operator()(ast::expr::IntegralConstantNode& node) const
                {
                    mp::cpp_int val = utils::ParseIntegralConstant<mp::cpp_int>(std::to_string(node.value));
                    mp::cpp_int max = std::numeric_limits<ast::expr::IntegralConstantNode::Type>::max();

                    if (val > max)
                    {
                        std::string why = "Integral constant overflow: constant " + val.str();
                        why += " is greater than the maximum representable value " + max.str() + ".";

                        throw IntegralConstantOverflowException(why);
                    }
                }

                void operator()(ast::expr::VariableNode&) const
                {
                }

                void operator()(ast::expr::BinaryOpNode& node) const
                {
                    AnalyzeSubExpr(node.lhs);
                    AnalyzeSubExpr(node.rhs);
                }

                void operator()(ast::expr::FunctionCallNode& node) const
                {
                    auto symbol = symbolTable.Lookup(std::to_string(node.name));
                    if (!symbol || !boost::apply_visitor(detail::IsSameExprType<SymbolTable::Function>(), *symbol))
                        throw UndefinedFunctionException("Function " + std::to_string(node.name) + " is undefined.");

                    for (auto& arg : node.args)
                    {
                        AnalyzeSubExpr(arg);
                    }
                }

            private:
                template <typename T>
                void AnalyzeSubExpr(T& expr) const
                {
                    boost::apply_visitor(Analyze(*this), expr);
                }

                ScopeView& symbolTable;
            };
        }

        namespace stmt
        {
            class Analyze : public boost::static_visitor<>
            {
            public:
                Analyze(ScopeView& table) : symbolTable(table)
                {
                }

                void operator()(ast::stmt::ExprNode& node) const
                {
                    AnalyzeSubExpr(node.expr, symbolTable);
                }

                void operator()(ast::stmt::ReturnNode& node) const
                {
                    AnalyzeSubExpr(node.expr, symbolTable);
                }

                void operator()(ast::stmt::VariableDefNode& node) const
                {
                    if (node.initialValueExpr)
                        AnalyzeSubExpr(*node.initialValueExpr, symbolTable);
                }

                void operator()(ast::stmt::IfElseNode& node) const
                {
                    auto outerScope = symbolTable.NextChild();
                    AnalyzeSubExpr(node.condition, outerScope);

                    auto ifBlockScope = outerScope.NextChild();
                    AnalyzeSubStmt(node.ifBlock, ifBlockScope);

                    auto elseBlockScope = outerScope.NextChild();
                    AnalyzeSubStmt(node.elseBlock, elseBlockScope);
                }

                void operator()(ast::stmt::ForLoopNode& node) const
                {
                    auto outerScope = symbolTable.NextChild();
                    if (node.init)
                        AnalyzeSubStmt(*node.init, outerScope);

                    if (node.condition)
                        AnalyzeSubExpr(*node.condition, outerScope);

                    auto bodyScope = outerScope.NextChild();
                    AnalyzeSubStmt(node.body, bodyScope);
                    
                    if(node.step)
                        AnalyzeSubExpr(*node.step, bodyScope);
                }

                void operator()(ast::stmt::WhileLoopNode& node) const
                {
                    auto outerScope = symbolTable.NextChild();
                    AnalyzeSubExpr(node.condition, outerScope);

                    auto bodyScope = symbolTable.NextChild();
                    AnalyzeSubStmt(node.body, bodyScope);
                }

                void operator()(ast::stmt::BlockNode& node) const
                {
                    for (auto& s : node.statements)
                    {
                        AnalyzeSubStmt(s, symbolTable);
                    }
                }

                void operator()(ast::stmt::ScopeNode& node) const
                {
                    auto outerScope = symbolTable.NextChild();
                    (Analyze(outerScope))(node.body);
                }

            private:
                template <typename T>
                void AnalyzeSubExpr(T& expr, ScopeView& table) const
                {
                    boost::apply_visitor(expr::Analyze(table), expr);
                }

                template <typename T>
                void AnalyzeSubStmt(T& stmt, ScopeView& table) const
                {
                    boost::apply_visitor(Analyze(table), stmt);
                }

                ScopeView& symbolTable;
            };
        }

        namespace module
        {
            class Analyze : public boost::static_visitor<>
            {
            public:
                Analyze(ScopeView& table) : symbolTable(table)
                {
                }

                void operator()(ast::module::FunctionNode& node) const
                {
                    auto functionScope = symbolTable.NextChild();
                    for (auto& arg : node.args)
                    {
                        (stmt::Analyze(functionScope))(arg);
                    }

                    (stmt::Analyze(functionScope))(node.body);
                }

            private:
                template <typename T>
                void AnalyzeSubStmt(T& stmt, ScopeView& table) const
                {
                    boost::apply_visitor(stmt::Analyze(table), stmt);
                }

                ScopeView& symbolTable;
            };
        }
    }

    void SemanticAnalyzer::operator()(ast::Module& module)
    {
        ScopeView ScopeView(symbolTable);
        for (auto& e : module.entries)
        {
            boost::apply_visitor(visitor::module::Analyze(ScopeView), e);
        }
    }
}
