#include <bolt/symboltable.hpp>
#include <bolt/lexer/lexer.hpp>
#include <bolt/parser/exceptions.hpp>
#include <bolt/parser/exprparser.hpp>
#include <bolt/parser/stmtparser.hpp>
#include <bolt/parser/utils.hpp>

namespace bolt
{
    namespace detail
    {
        ast::stmt::IfElseNode StmtParser::ParseIfElse(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            utils::parser::Expect(If, it);

            symbolTable.Push();

            utils::parser::Expect(ParenthesisOpen, it);
            auto cond = ExprParser::ParseExpr(symbolTable, it, end);
            utils::parser::Expect(ParenthesisClose, it);

            auto ifBlock = ParseScope(symbolTable, it, end);

            utils::parser::Expect(Else, it);
            auto elseBlock = ParseScope(symbolTable, it, end);

            symbolTable.Pop();

            return ast::stmt::IfElseNode { cond, ifBlock, elseBlock };
        }

        ast::stmt::ForLoopNode StmtParser::ParseForLoop(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            utils::parser::Expect(For, it);

            symbolTable.Push();

            utils::parser::Expect(ParenthesisOpen, it);
            boost::optional<ast::stmt::Node> init = boost::none;
            if ((*it).type != Semicolon)
                init = ParseStmt(symbolTable, it, end);

            boost::optional<ast::expr::Node> cond = boost::none;
            if ((*it).type != Semicolon)
                cond = ExprParser::ParseExpr(symbolTable, it, end);

            utils::parser::Expect(Semicolon, it);

            boost::optional<ast::expr::Node> step;
            if ((*it).type != Semicolon)
                step = ExprParser::ParseExpr(symbolTable, it, end);
            
            utils::parser::Expect(ParenthesisClose, it);

            auto body = ParseScope(symbolTable, it, end);

            symbolTable.Pop();

            return ast::stmt::ForLoopNode { init, cond, step, body };
        }

        ast::stmt::WhileLoopNode StmtParser::ParseWhileLoop(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            utils::parser::Expect(While, it);

            symbolTable.Push();

            utils::parser::Expect(ParenthesisOpen, it);
            auto cond = ExprParser::ParseExpr(symbolTable, it, end);
            utils::parser::Expect(ParenthesisClose, it);

            auto body = ParseScope(symbolTable, it, end);

            symbolTable.Pop();

            return ast::stmt::WhileLoopNode { cond, body };
        }

        ast::stmt::ReturnNode StmtParser::ParseReturnStmt(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            utils::parser::Expect(Return, it);
            auto returnStmt = ast::stmt::ReturnNode { ExprParser::ParseExpr(symbolTable, it, end) };
            utils::parser::Expect(Semicolon, it);

            return returnStmt;
        }

        ast::stmt::BlockNode StmtParser::ParseBlock(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            utils::parser::Expect(CurlyBraceOpen, it);

            std::vector<ast::stmt::Node> stmts;
            while ((*it).type != CurlyBraceClose)
            {
                stmts.push_back(ParseStmt(symbolTable, it, end));
            }

            utils::parser::Expect(CurlyBraceClose, it);

            return ast::stmt::BlockNode { stmts };
        }

        ast::stmt::ScopeNode StmtParser::ParseScope(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            symbolTable.Push();
            auto body = ParseBlock(symbolTable, it, end);
            symbolTable.Pop();

            return ast::stmt::ScopeNode { body };
        }

        ast::stmt::VariableDefNode StmtParser::ParseVariableDef(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            if (!utils::parser::IsTypeToken((*it).type))
                throw exceptions::SyntaxErrorException("Expected an type.");

            auto type = (*it).value;
            ++it;

            if ((*it).type != Identifier)
                throw exceptions::SyntaxErrorException("Expected an identifier.");

            auto name = (*it).value;
            ++it;

            if (symbolTable.Find(std::to_string(name)))
                throw exceptions::SymbolRedeclarationException(std::to_string(name));

            symbolTable.Add(std::to_string(name), SymbolTable::Variable { std::to_string(type), std::to_string(name) });

            boost::optional<ast::expr::Node> initValue = boost::none;
            if ((*it).type != Semicolon)
            {
                utils::parser::Expect(Assign, it);
                initValue = ExprParser::ParseExpr(symbolTable, it, end);
            }

            utils::parser::Expect(Semicolon, it);

            return ast::stmt::VariableDefNode { type, name, initValue };
        }

        ast::stmt::ExprNode StmtParser::ParseTopLevelExpr(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            auto expr = ExprParser::ParseExpr(symbolTable, it, end);
            utils::parser::Expect(Semicolon, it);

            return ast::stmt::ExprNode { expr };
        }

        ast::stmt::Node StmtParser::ParseStmt(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            Token token = *it;

            if (token.type == If)
                return ParseIfElse(symbolTable, it, end);
            else if (token.type == While)
                return ParseWhileLoop(symbolTable, it, end);
            else if (token.type == For)
                return ParseForLoop(symbolTable, it, end);
            else if (token.type == Return)
                return ParseReturnStmt(symbolTable, it, end);
            else if (token.type == CurlyBraceOpen)
                return ParseScope(symbolTable, it, end);
            else if (utils::parser::IsTypeToken(token.type) && utils::parser::IsNextToken(it, end, Identifier))
                return ParseVariableDef(symbolTable, it, end);
            else
                return ParseTopLevelExpr(symbolTable, it, end);
        }
    }
}
