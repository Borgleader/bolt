#include <bolt/symboltable.hpp>
#include <bolt/lexer/lexer.hpp>
#include <bolt/parser/exceptions.hpp>
#include <bolt/parser/moduleparser.hpp>
#include <bolt/parser/stmtparser.hpp>
#include <bolt/parser/utils.hpp>

namespace bolt
{
    namespace detail
    {
        ast::module::Node ModuleParser::ParseFunction(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            if (!utils::parser::IsTypeToken(static_cast<TokenType>((*it).type)))
                throw exceptions::SyntaxErrorException("Expected an type.");

            auto type = (*it).value;
            ++it;

            if ((*it).type != Identifier)
                throw exceptions::SyntaxErrorException("Expected an identifier.");

            auto name = (*it).value;
            ++it;

            if (symbolTable.Find(std::to_string(name)))
                throw exceptions::SymbolRedeclarationException(std::to_string(name));

            // Introduce the scope before the argument list because arguments should not be overriden by
            // top level variable declarations.
            symbolTable.Push();

            std::vector<ast::stmt::VariableDefNode> args;
            std::vector<std::string> argTypes;
            utils::parser::Expect(ParenthesisOpen, it);

            if ((*it).type != ParenthesisClose)
            {
                for (;;)
                {
                    auto arg = StmtParser::ParseVariableDef(symbolTable, it, end);
                    argTypes.push_back(std::to_string(arg.type));
                    args.push_back(arg);

                    if ((*it).type == Comma)
                        ++it;
                    else
                        break;
                } 
            }

            utils::parser::Expect(ParenthesisClose, it);

            auto body = StmtParser::ParseBlock(symbolTable, it, end);

            symbolTable.Pop();
            symbolTable.Add(std::to_string(name), SymbolTable::Function { std::to_string(type), std::to_string(name), argTypes });

            return ast::module::FunctionNode { type, name, args, body };
        }

        ast::Module ModuleParser::ParseModule(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            std::vector<ast::module::Node> entries;
            while (it != end)
            {
                entries.push_back(ParseFunction(symbolTable, it, end));
            }

            return { entries };
        }
    }
}
