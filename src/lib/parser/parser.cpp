#include <bolt/symboltable.hpp>
#include <bolt/lexer/lexer.hpp>
#include <bolt/parser/exceptions.hpp>
#include <bolt/parser/moduleparser.hpp>
#include <bolt/parser/parser.hpp>

namespace bolt
{
    Parser::Parser(const std::string& code) : code(code)
    {
    }

    boost::optional<ast::Module> Parser::Parse(SymbolTable& table)
    {
        char const* inputIt = code.c_str();
        char const* inputEnd = &inputIt[code.size()];

        Lexer<LexerType> tokenLexer(inputIt);
        TokenIterator it = tokenLexer.begin(inputIt, inputEnd);
        TokenIterator end = tokenLexer.end();

        try
        {
            // Skip leading whitespace
            for (; it->id() == Whitespace; ++it);

            LexerIterator lexIt(tokenLexer, it);
            LexerIterator lexEnd(tokenLexer, end);

            ScopeView globalScope(table);
            auto module = detail::ModuleParser::ParseModule(globalScope, lexIt, lexEnd);
            
            if (lexIt == lexEnd)
                return module;
            else
                return boost::none;
        }
        catch (exceptions::SyntaxErrorException& e)
        {
            std::cerr << "SyntaxError: " << std::endl;
            std::cerr << e.what() << std::endl;

            return boost::none;
        }
        catch (std::exception& e)
        {
            std::cerr << e.what() << std::endl;
            return boost::none;
        }
    }
}
