#include <bolt/symboltable.hpp>
#include <bolt/lexer/lexer.hpp>
#include <bolt/parser/exceptions.hpp>
#include <bolt/parser/exprparser.hpp>
#include <bolt/parser/utils.hpp>

namespace bolt
{
    namespace detail
    {
        ast::expr::Node ExprParser::ParseTerm(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            Token token = *it;
            switch(token.type)
            {
                case BinLiteral:
                case OctLiteral:
                case HexLiteral:
                case DecLiteral:
                {
                    ++it;
                    return ast::expr::IntegralConstantNode { token.value };
                }
                case Identifier:
                {
                    auto name = (*it).value;
                    ++it;

                    if (symbolTable.Lookup(std::to_string(name)))
                        throw exceptions::UndeclaredVariableException(std::to_string(name));

                    return ast::expr::VariableNode { name };
                }
                case ParenthesisOpen:
                {
                    ++it;
                    auto subExpr = ParseExpr(symbolTable, it, end);
                    utils::parser::Expect(ParenthesisClose, it);
                    return subExpr;
                }
                default:
                {
                    throw exceptions::SyntaxErrorException("Expected primary expression.");
                }
            }
        }

        ast::expr::Node ExprParser::ParseFunctionCall(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            if ((*it).type == Identifier && utils::parser::IsNextToken(it, end, ParenthesisOpen))
            {
                auto name = (*it).value;
                ++it;

                utils::parser::Expect(ParenthesisOpen, it);

                std::vector<ast::expr::Node> args;
                while ((*it).type != ParenthesisClose)
                {
                    args.push_back(ParseTerm(symbolTable, it, end));

                    if ((*it).type == Comma)
                        ++it;
                }

                utils::parser::Expect(ParenthesisClose, it);

                return ast::expr::FunctionCallNode { name, args };
            }
            else
            {
                return ParseTerm(symbolTable, it, end);
            }
        }

        ast::expr::Node ExprParser::ParseMulDiv(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            auto lhs = ParseFunctionCall(symbolTable, it, end);

            for (;;)
            {
                TokenType currToken = (*it).type;
                if (currToken == Multiply || currToken == Divide)
                {
                    ++it;
                    auto rhs = ParseFunctionCall(symbolTable, it, end);
                    lhs = ast::expr::BinaryOpNode { lhs, rhs, currToken };
                }
                else
                {
                    break;
                }
            }

            return lhs;
        }

        ast::expr::Node ExprParser::ParseAddSub(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            auto lhs = ParseMulDiv(symbolTable, it, end);

            for (;;)
            {
                TokenType currToken = (*it).type;
                if (currToken == Add || currToken == Subtract)
                {
                    ++it;
                    auto rhs = ParseMulDiv(symbolTable, it, end);
                    lhs = ast::expr::BinaryOpNode { lhs, rhs, currToken };
                }
                else
                {
                    break;
                }
            }

            return lhs;
        }

        ast::expr::Node ExprParser::ParseGreaterLower(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            auto lhs = ParseAddSub(symbolTable, it, end);

            for (;;)
            {
                TokenType currToken = (*it).type;
                if (currToken == Lower || currToken == LowerEqual || currToken == Greater || currToken == GreaterEqual)
                {
                    ++it;
                    auto rhs = ParseAddSub(symbolTable, it, end);
                    lhs = ast::expr::BinaryOpNode { lhs, rhs, currToken };
                }
                else
                {
                    break;
                }
            }

            return lhs;
        }

        ast::expr::Node ExprParser::ParseEqualNotEqual(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            auto lhs = ParseGreaterLower(symbolTable, it, end);

            for (;;)
            {
                TokenType currToken = (*it).type;
                if (currToken == Equal || currToken == NotEqual)
                {
                    ++it;
                    auto rhs = ParseGreaterLower(symbolTable, it, end);
                    lhs = ast::expr::BinaryOpNode { lhs, rhs, currToken };
                }
                else
                {
                    break;
                }
            }

            return lhs;
        }

        ast::expr::Node ExprParser::ParseAssign(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            auto lhs = ParseEqualNotEqual(symbolTable, it, end);

            if ((*it).type == Assign)
            {
                ++it;
                auto rhs = ParseEqualNotEqual(symbolTable, it, end);
                lhs = ast::expr::BinaryOpNode { lhs, rhs, Assign };
            }

            return lhs;
        }

        ast::expr::Node ExprParser::ParseExpr(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end)
        {
            return ParseAssign(symbolTable, it, end);
        }
    }
}
