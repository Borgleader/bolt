#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <bolt/symboltable.hpp>
#include <bolt/parser/parser.hpp>
#include <bolt/semantic/semanticanalyzer.hpp>

std::string ReadFile(const std::ifstream& in)
{
    std::stringstream sstr;
    sstr << in.rdbuf();
    return sstr.str();
}

int main(int argc, char* argv[])
{
    std::cout << "bolt LLVM compiler" << std::endl << std::endl;
    if (argc < 2)
    {
        std::cerr << "No input file specified. Quitting..." << std::endl;
        return -1;
    }

    std::ifstream ifs(argv[1]);
    std::string code = ReadFile(ifs);

    bolt::SymbolTable symbolTable;
    auto module = bolt::Parser(code).Parse(symbolTable);
    std::cout << (module ? "Parse successful." : "Parsing failure.") << std::endl;
    
    (bolt::SemanticAnalyzer(symbolTable))(*module);

    std::cout << "Quitting..." << std::endl;
    return 0;
}
