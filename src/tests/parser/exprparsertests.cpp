#include <string>

#include <boost/test/unit_test.hpp>

#include <bolt/symboltable.hpp>
#include <bolt/lexer/lexer.hpp>
#include <bolt/parser/exprparser.hpp>

struct ParserHelper
{
    ParserHelper(const std::string& code) : inputIt(code.c_str()), inputEnd(&inputIt[code.size()]), tokenLexer(inputIt),
        it(tokenLexer.begin(inputIt, inputEnd)), end(tokenLexer.end()), lexIt(tokenLexer, it), lexEnd(tokenLexer, end)
    {
    }

private:
    char const* inputIt;
    char const* inputEnd;

    bolt::Lexer<bolt::LexerType> tokenLexer;
    bolt::TokenIterator it;
    bolt::TokenIterator end;

public:
    bolt::LexerIterator lexIt;
    bolt::LexerIterator lexEnd;
};


BOOST_AUTO_TEST_SUITE(Expression_Parsing)

BOOST_AUTO_TEST_CASE(Single_Literal)
{
    ParserHelper helper("42");

    bolt::SymbolTable symbolTable;
    bolt::ScopeView globalScope(symbolTable);

    bolt::detail::ExprParser::ParseExpr(globalScope, helper.lexIt, helper.lexEnd);
    BOOST_REQUIRE(helper.lexIt == helper.lexEnd);
}

BOOST_AUTO_TEST_CASE(Binary_Operation)
{
    ParserHelper helper("a + b");

    bolt::SymbolTable symbolTable;
    bolt::ScopeView globalScope(symbolTable);

    bolt::detail::ExprParser::ParseExpr(globalScope, helper.lexIt, helper.lexEnd);
    BOOST_REQUIRE(helper.lexIt == helper.lexEnd);
}

BOOST_AUTO_TEST_CASE(Expression_With_Parentheses)
{
    ParserHelper helper("(a + b) * c");

    bolt::SymbolTable symbolTable;
    bolt::ScopeView globalScope(symbolTable);

    bolt::detail::ExprParser::ParseExpr(globalScope, helper.lexIt, helper.lexEnd);
    BOOST_REQUIRE(helper.lexIt == helper.lexEnd);
}

BOOST_AUTO_TEST_CASE(Expression_Wrapped_In_Parentheses)
{
    ParserHelper helper("((a + b) * c)");

    bolt::SymbolTable symbolTable;
    bolt::ScopeView globalScope(symbolTable);

    bolt::detail::ExprParser::ParseExpr(globalScope, helper.lexIt, helper.lexEnd);
    BOOST_REQUIRE(helper.lexIt == helper.lexEnd);
}

BOOST_AUTO_TEST_SUITE_END()
