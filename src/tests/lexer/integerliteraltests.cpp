#include <algorithm>
#include <string>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <bolt/lexer/lexer.hpp>

std::vector<bolt::TokenType> Lex(std::string code)
{
    char const* inputIt = code.c_str();
    char const* inputEnd = &inputIt[code.size()];

    bolt::Lexer<bolt::LexerType> tokenLexer(inputIt);
    bolt::TokenIterator it = tokenLexer.begin(inputIt, inputEnd);
    bolt::TokenIterator end = tokenLexer.end();

    std::vector<bolt::TokenType> tokens;
    while(it != end)
    {
        tokens.push_back(static_cast<bolt::TokenType>(it->id()));
        ++it;
    }

    auto removedIt = std::remove_if(tokens.begin(), tokens.end(), [](bolt::TokenType t) { return t == bolt::Whitespace; });
    tokens.erase(removedIt, tokens.end());
    
    return tokens;
}

BOOST_AUTO_TEST_SUITE(Integer_Literal_Lexing)

// Binary literal tests
BOOST_AUTO_TEST_CASE(Binary_Literal)
{
    auto tokens = Lex("0b1010");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::BinLiteral);
}

BOOST_AUTO_TEST_CASE(Binary_Literal_Single_Digit)
{
    auto tokens = Lex("0b1");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::BinLiteral);
}

BOOST_AUTO_TEST_CASE(Binary_Literal_Leading_Zeros)
{
    auto tokens = Lex("0b001010");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::BinLiteral);
}

BOOST_AUTO_TEST_CASE(Binary_Literal_Only_Zeros)
{
    auto tokens = Lex("0b00000000");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::BinLiteral);
}

BOOST_AUTO_TEST_CASE(Binary_Literal_Invalid_Digits)
{
    auto tokens = Lex("0b1210");

    BOOST_REQUIRE(tokens.size() > 1);
}

// Octal literal tests
BOOST_AUTO_TEST_CASE(Octal_Literal)
{
    auto tokens = Lex("0o1357");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::OctLiteral);
}

BOOST_AUTO_TEST_CASE(Octal_Literal_Single_Digit)
{
    auto tokens = Lex("0o7");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::OctLiteral);
}

BOOST_AUTO_TEST_CASE(Octal_Literal_Leading_Zeros)
{
    auto tokens = Lex("0o002466");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::OctLiteral);
}

BOOST_AUTO_TEST_CASE(Octal_Literal_Only_Zeros)
{
    auto tokens = Lex("0o00000000");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::OctLiteral);
}

BOOST_AUTO_TEST_CASE(Octal_Literal_Invalid_Digits)
{
    auto tokens = Lex("0o2468");

    BOOST_REQUIRE(tokens.size() > 1);
}

// Decimal literal tests
BOOST_AUTO_TEST_CASE(Decimal_Literal)
{
    auto tokens = Lex("0d13579");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::DecLiteral);
}

BOOST_AUTO_TEST_CASE(Decimal_Literal_No_Prefix)
{
    auto tokens = Lex("13579");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::DecLiteral);
}

BOOST_AUTO_TEST_CASE(Decimal_Literal_Single_Digit)
{
    auto tokens = Lex("0d9");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::DecLiteral);
}

BOOST_AUTO_TEST_CASE(Decimal_Literal_Single_Digit_No_Prefix)
{
    auto tokens = Lex("9");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::DecLiteral);
}

BOOST_AUTO_TEST_CASE(Decimal_Literal_Leading_Zeros)
{
    auto tokens = Lex("0d002468");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::DecLiteral);
}

BOOST_AUTO_TEST_CASE(Decimal_Literal_Leading_Zeros_No_Prefix)
{
    auto tokens = Lex("002468");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::DecLiteral);
}

BOOST_AUTO_TEST_CASE(Decimal_Literal_Only_Zeros)
{
    auto tokens = Lex("0d00000000");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::DecLiteral);
}

BOOST_AUTO_TEST_CASE(Decimal_Literal_Only_Zeros_No_Prefix)
{
    auto tokens = Lex("00000000");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::DecLiteral);
}

BOOST_AUTO_TEST_CASE(Decimal_Literal_Invalid_Digits)
{
    auto tokens = Lex("0o369B");

    BOOST_REQUIRE(tokens.size() > 1);
}

BOOST_AUTO_TEST_CASE(Decimal_Literal_Invalid_Digits_No_Prefix)
{
    auto tokens = Lex("369B");

    BOOST_REQUIRE(tokens.size() > 1);
}

// Hexadecimal literal tests
BOOST_AUTO_TEST_CASE(Hexadecimal_Literal)
{
    auto tokens = Lex("0x369CF");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::HexLiteral);
}

BOOST_AUTO_TEST_CASE(Hexadecimal_Literal_Single_Digit)
{
    auto tokens = Lex("0xF");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::HexLiteral);
}

BOOST_AUTO_TEST_CASE(Hexadecimal_Literal_Leading_Zeros)
{
    auto tokens = Lex("0x0048D");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::HexLiteral);
}

BOOST_AUTO_TEST_CASE(Hexadecimal_Literal_Only_Zeros)
{
    auto tokens = Lex("0x00000000");

    BOOST_REQUIRE_EQUAL(tokens.size(), 1);
    BOOST_REQUIRE_EQUAL(tokens[0], bolt::HexLiteral);
}

BOOST_AUTO_TEST_CASE(Hexadecimal_Literal_Invalid_Digits)
{
    auto tokens = Lex("0x47AG");

    BOOST_REQUIRE(tokens.size() > 1);
}

BOOST_AUTO_TEST_CASE(Hexadecimal_Literal_Invalid_Digit_Case)
{
    auto tokens = Lex("0x48d");

    BOOST_REQUIRE(tokens.size() > 1);
}

BOOST_AUTO_TEST_SUITE_END()
