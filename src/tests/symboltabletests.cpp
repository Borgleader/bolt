#include <boost/test/unit_test.hpp>

#include <bolt/symboltable.hpp>

BOOST_AUTO_TEST_SUITE(Lookup)

BOOST_AUTO_TEST_CASE(Global_Scope)
{
    bolt::SymbolTable symbolTable;
    bolt::ScopeView view(symbolTable);

    BOOST_REQUIRE(!view.Lookup("foo"));
    BOOST_REQUIRE(!view.Lookup("bar"));
    BOOST_REQUIRE(!view.Lookup("baz"));

    view.Add("foo", bolt::SymbolTable::Variable { "int8", "foo" });
    view.Add("bar", bolt::SymbolTable::Function { "void", "bar", {} });

    BOOST_REQUIRE(view.Lookup("foo"));
    BOOST_REQUIRE(view.Lookup("bar"));
    BOOST_REQUIRE(!view.Lookup("baz"));
}

BOOST_AUTO_TEST_CASE(Nested_Scope)
{
    bolt::SymbolTable symbolTable;
    
    bolt::ScopeView view(symbolTable);
    view.Add("foo", bolt::SymbolTable::Variable { "int8", "foo" });

    {
        view.Push();
        view.Add("bar", bolt::SymbolTable::Variable { "int8", "bar" });
        view.Pop();
    }

    BOOST_REQUIRE(view.Lookup("foo"));
    BOOST_REQUIRE(!view.Lookup("bar"));

    auto scope = view.NextChild();
    BOOST_REQUIRE(scope.Lookup("foo"));
    BOOST_REQUIRE(scope.Lookup("bar"));
}

BOOST_AUTO_TEST_CASE(Sibling_Scope)
{
    bolt::SymbolTable symbolTable;
    
    bolt::ScopeView view(symbolTable);
    view.Add("foo", bolt::SymbolTable::Variable { "int8", "foo" });

    {
        view.Push();
        view.Add("bar", bolt::SymbolTable::Variable { "int8", "bar" });
        view.Pop();
    }

    {
        view.Push();
        view.Add("baz", bolt::SymbolTable::Variable { "int8", "baz" });
        view.Pop();       
    }

    BOOST_REQUIRE(view.Lookup("foo"));
    BOOST_REQUIRE(!view.Lookup("bar"));
    BOOST_REQUIRE(!view.Lookup("baz"));

    auto firstScope = view.NextChild();
    BOOST_REQUIRE(firstScope.Lookup("foo"));
    BOOST_REQUIRE(firstScope.Lookup("bar"));
    BOOST_REQUIRE(!firstScope.Lookup("baz"));

    auto secondScope = view.NextChild();
    BOOST_REQUIRE(secondScope.Lookup("foo"));
    BOOST_REQUIRE(!secondScope.Lookup("bar"));
    BOOST_REQUIRE(secondScope.Lookup("baz"));
}

BOOST_AUTO_TEST_SUITE_END()
