#!/usr/bin/python
import ninja_syntax
import itertools
import os
import fnmatch
import re
import sys
import argparse

# --- util functions
def flags(*iterables):
    return ' '.join(itertools.chain(*iterables))

def include(d):
    return '-I' + d;

def dependency_include(d):
    return '-isystem' + os.path.join('deps', d, 'include');

def dependency_link(l):
    return '-l' + l;

def get_files(root, pattern):
    pattern = fnmatch.translate(pattern)
    for dir, dirs, files in os.walk(root):
        for f in files:
            if re.match(pattern, f):
                yield os.path.join(dir, f)

def object_file(fn):
    return os.path.join('obj', re.sub(r'\.cpp', '.o', fn))

# --- variables
dependencies = ['llvm', 'llvm-build']
libdependencies = ['boost_unit_test_framework']
include_flags = flags([include('include')], map(dependency_include, dependencies))
cxx_flags = flags(['-Wall', '-Wextra', '-std=c++11', '-g', '`deps/llvm-build/bin/llvm-config --cppflags`'])
ld_flags = flags(['-flto', '-pthread', '`deps/llvm-build/bin/llvm-config --ldflags --libs`', '-ldl', '-lm', '-ltinfo'], map(dependency_link, libdependencies))

parser = argparse.ArgumentParser()
parser.add_argument('--cxx', default='g++', metavar='executable', help='compiler name to use (default: g++)')
args = parser.parse_args()

# ---
ninja = ninja_syntax.Writer(open('build.ninja', 'w'))

ninja.variable('ninja_required_version', '1.5')
ninja.variable('builddir', 'obj')

# --- rules
ninja.rule('bootstrap',
        command = ' '.join(sys.argv),
        generator = True,
        description = 'BOOTSTRAP')

ninja.rule('cxx',
        command = args.cxx + ' -MMD -MF $out.d -c ' + cxx_flags + ' ' + include_flags + ' $in -o $out',
        deps = 'gcc',
        depfile = '$out.d',
        description = 'C++ $in')

ninja.rule('link',
        command = args.cxx + ' ' + cxx_flags + '  $in ' + ld_flags + ' -o $out',
        description = 'LINK $in')

ninja.rule('lib',
        command = 'ar rc $out $in && ranlib $out',
        description = 'AR $in')

# --- build edges
ninja.build('build.ninja', 'bootstrap', implicit = sys.argv[0])

lib_src_files = list(get_files('src/lib', '*.cpp'))
lib_obj_files = [object_file(fn) for fn in lib_src_files]
for fn in lib_src_files:
    ninja.build(object_file(fn), 'cxx', inputs = fn)

test_src_files = list(get_files('src/tests', '*.cpp'))
test_obj_files = [object_file(fn) for fn in test_src_files]
for fn in test_src_files:
    ninja.build(object_file(fn), 'cxx', inputs = fn)

src_files = list(get_files('src/frontend', '*.cpp'))
obj_files = [object_file(fn) for fn in src_files]
for fn in src_files:
    ninja.build(object_file(fn), 'cxx', inputs = fn)

boltlib = 'bin/boltlib'
ninja.build(boltlib, 'lib', inputs = lib_obj_files)

bolt = 'bin/bolt'
ninja.build(bolt, 'link', inputs = obj_files + [boltlib])

tests = 'bin/tests'
ninja.build(tests, 'link', inputs = test_obj_files + [boltlib])

all = 'all'
ninja.build(all, 'phony', inputs = [bolt, tests])

# --- default targets
ninja.default(bolt)
