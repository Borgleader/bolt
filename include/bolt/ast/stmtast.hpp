#pragma once

#include <vector>

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <bolt/ast/exprast.hpp>

namespace bolt
{
    namespace ast
    {
        namespace stmt
        {
            struct ExprNode
            {
                expr::Node expr;
            };

            struct ReturnNode
            {
                expr::Node expr;
            };

            struct VariableDefNode
            {
                TokenIterRange type;
                TokenIterRange name;
                boost::optional<expr::Node> initialValueExpr;
            };

            struct IfElseNode;
            struct ForLoopNode;
            struct WhileLoopNode;
            struct BlockNode;
            struct ScopeNode;

            typedef boost::variant<
                ExprNode,
                ReturnNode,
                VariableDefNode,
                boost::recursive_wrapper<IfElseNode>,
                boost::recursive_wrapper<ForLoopNode>,
                boost::recursive_wrapper<WhileLoopNode>,
                boost::recursive_wrapper<BlockNode>,
                boost::recursive_wrapper<ScopeNode>
            > Node;

            struct IfElseNode
            {
                expr::Node condition;
                Node ifBlock;
                Node elseBlock;
            };

            struct ForLoopNode
            {
                boost::optional<Node> init;
                boost::optional<expr::Node> condition;
                boost::optional<expr::Node> step;
                Node body;
            };

            struct WhileLoopNode
            {
                expr::Node condition;
                Node body;
            };

            struct BlockNode
            {
                std::vector<Node> statements;
            };

            // Unlike BlockNode, this does introduces a scope
            struct ScopeNode
            {
                BlockNode body;
            };
        }
    }
}
