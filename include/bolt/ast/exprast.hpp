#pragma once

#include <cstdint>
#include <memory>
#include <vector>

#include <boost/variant.hpp>

#include <bolt/lexer/token.hpp>

namespace bolt
{
    namespace ast
    {
        namespace expr
        {
            struct IntegralConstantNode
            {
                typedef std::int32_t Type;

                TokenIterRange value;
            };

            struct VariableNode
            {
                TokenIterRange name;
            };

            struct BinaryOpNode;
            struct FunctionCallNode;

            typedef boost::variant<
                IntegralConstantNode,
                VariableNode,
                boost::recursive_wrapper<BinaryOpNode>,
                boost::recursive_wrapper<FunctionCallNode>
            > Node;

            struct BinaryOpNode
            {
                Node lhs, rhs;
                TokenType op;
            };

            struct FunctionCallNode
            {
                TokenIterRange name;
                std::vector<Node> args;
            };
        }
    }
}
