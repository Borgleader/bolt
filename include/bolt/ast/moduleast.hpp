#pragma once

#include <vector>

#include <boost/variant.hpp>

#include <bolt/ast/stmtast.hpp>

namespace bolt
{
    namespace ast
    {
        namespace module
        {
            struct FunctionNode
            {
                TokenIterRange returnType;
                TokenIterRange name;
                std::vector<ast::stmt::VariableDefNode> args;
                ast::stmt::BlockNode body;
            };

            typedef boost::variant<
                FunctionNode
            > Node;
        }

        struct Module
        {
            std::vector<module::Node> entries;
        };
    }
}
