#pragma once

#include <string>

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/lex_lexertl.hpp>

namespace bolt
{
    enum TokenType
    {
        Whitespace = boost::spirit::lex::min_token_id,

        // Keywords
        Var,
        If,
        Else,
        For,
        While,
        Return,

        // Types
        Bool,
        Int8,
        Int16,
        Int32,
        Int64,
        String,
        UInt8,
        UInt16,
        UInt32,
        UInt64,
        Void,

        Identifier,

        // Integral literals
        DecLiteral,
        BinLiteral,
        OctLiteral,
        HexLiteral,

        // String literals
        StringLiteral,

        // Operators
        Add,
        Subtract,
        Multiply,
        Divide,
        LowerEqual,
        Lower,
        GreaterEqual,
        Greater,
        Equal,
        NotEqual,
        Assign,

        ParenthesisOpen,
        ParenthesisClose,
        CurlyBraceOpen,
        CurlyBraceClose,

        Semicolon,
        Comma,

        // Enum count, do not add anything past this point
        NumTokenTypes,
    };

    typedef boost::spirit::lex::lexertl::token<char const*> LexToken;
    typedef boost::iterator_range<char const*> TokenIterRange;

    struct Token
    {
        TokenType type;
        TokenIterRange value;

        size_t row;
        size_t column;
    };
}

namespace std
{
    std::string to_string(bolt::TokenType t);
    std::string to_string(bolt::TokenIterRange r);
    std::string to_string(const bolt::Token& t);
}
