#pragma once

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/lex_lexertl.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_statement.hpp>
#include <boost/spirit/include/phoenix_algorithm.hpp>
#include <boost/spirit/include/phoenix_core.hpp>

#include <bolt/lexer/token.hpp>

namespace bolt
{
    template <typename T>
    struct Lexer : boost::spirit::lex::lexer<T>
    {
        Lexer(char const* start) : row(1), rowStart(start)
        {
            using boost::spirit::lex::token_def;
            using boost::phoenix::ref;
            using boost::spirit::lex::_end;

            this->self
                // Whitespace
                = token_def<>("[\n]", Whitespace)[++ref(row), ref(rowStart) = _end]
                | token_def<>("[ \r\t]", Whitespace)

                // Keywords must appear before identifier, or else they get tagged as identifiers
                | token_def<>("var", Var)
                | token_def<>("if", If)
                | token_def<>("else", Else)
                | token_def<>("for", For)
                | token_def<>("while", While)
                | token_def<>("return", Return)

                // Types like keywords must appear before identifier
                | token_def<>("bool", Bool)
                | token_def<>("int8", Int8)
                | token_def<>("int16", Int16)
                | token_def<>("int32", Int32)
                | token_def<>("int64", Int64)
                | token_def<>("string", String)
                | token_def<>("uint8", UInt8)
                | token_def<>("uint16", UInt16)
                | token_def<>("uint32", UInt32)
                | token_def<>("uint64", UInt64)
                | token_def<>("void", Void)

                | token_def<>("[a-zA-Z][a-zA-Z0-9]*", Identifier)

                | token_def<>("0b[0-1]+", BinLiteral)
                | token_def<>("0o[0-7]+", OctLiteral)
                | token_def<>("0x[0-9A-F]+", HexLiteral)
                | token_def<>("(0d)?[0-9]+", DecLiteral)

                | token_def<>("\\\".*\\\"", StringLiteral)

                | token_def<>("\\+", Add)
                | token_def<>("\\-", Subtract)
                | token_def<>("\\*", Multiply)
                | token_def<>("\\/", Divide)
                | token_def<>("<=", LowerEqual)
                | token_def<>("<", Lower)
                | token_def<>(">=", GreaterEqual)
                | token_def<>(">", Greater)
                | token_def<>("==", Equal)
                | token_def<>("!=", NotEqual)
                | token_def<>("=", Assign)

                | token_def<>("\\(", ParenthesisOpen)
                | token_def<>("\\)", ParenthesisClose)
                | token_def<>("\\{", CurlyBraceOpen)
                | token_def<>("\\}", CurlyBraceClose)

                | token_def<>(";", Semicolon)
                | token_def<>(",", Comma);
        }

        size_t row;
        char const* rowStart;
    };

    typedef boost::spirit::lex::lexertl::actor_lexer<LexToken> LexerType;
    typedef LexerType::iterator_type TokenIterator;

    struct LexerIterator
    {
        LexerIterator(Lexer<LexerType>& lexer, TokenIterator& it) : lexer(lexer), it(it)
        {}

        LexerIterator operator++(int)
        {
            auto copy = *this;
            Increment();
            return copy;
        }

        LexerIterator& operator++()
        {
            Increment();
            return *this;
        }

        Token operator*()
        {
            return CurrentToken();
        }

        bool operator==(const LexerIterator& other) const
        {
            return it == other.it;
        }

        bool operator!=(const LexerIterator& other) const
        {
            return !((*this) == other);
        }

    private:
        // Move iterator forward once & and skip any whitespace
        void Increment()
        {
            ++it;
            for (; it->id() == Whitespace; ++it) {}
        }

        Token CurrentToken()
        {
            Token token = {
                static_cast<TokenType>(it->id()),
                it->value(),
                lexer.row,
                static_cast<std::size_t>(it->value().begin() - lexer.rowStart) + 1
            };

            return token;
        }

        Lexer<LexerType>& lexer;
        TokenIterator it;
    };
}
