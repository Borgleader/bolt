#pragma once

#include <bolt/ast/exprast.hpp>

namespace bolt
{
    struct LexerIterator;
    class ScopeView;

    namespace detail
    {
        class ExprParser
        {
        public:
            static ast::expr::Node ParseTerm(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::expr::Node ParseFunctionCall(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::expr::Node ParseMulDiv(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::expr::Node ParseAddSub(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::expr::Node ParseGreaterLower(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::expr::Node ParseEqualNotEqual(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::expr::Node ParseAssign(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::expr::Node ParseExpr(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);

        private:
            ExprParser() = delete;
        };
    }
}
