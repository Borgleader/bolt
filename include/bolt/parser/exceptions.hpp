#pragma once

#include <exception>
#include <string>

namespace bolt
{
    namespace exceptions
    {
        struct SyntaxErrorException : public std::exception
        {
            SyntaxErrorException() : why("")
            {
            }

            SyntaxErrorException(std::string why) : why(why)
            {
            }

            const char* what() const throw() override { return why.c_str(); }

        private:
            std::string why;
        };

        struct UndeclaredVariableException : public std::exception
        {
            UndeclaredVariableException() : why("")
            {
            }

            UndeclaredVariableException(std::string identifier) : 
                why("Variable " + identifier + " without a prior declaration.")
            {
            }

            const char* what() const throw() override { return why.c_str(); }

        private:
            std::string why;
        };

        struct SymbolRedeclarationException : public std::exception
        {
            SymbolRedeclarationException() : why("")
            {
            }

            SymbolRedeclarationException(std::string identifier) : 
                why("Symbol " + identifier + " was already declared in this scope.")
            {
            }

            const char* what() const throw() override { return why.c_str(); }

        private:
            std::string why;
        };
    }
}
