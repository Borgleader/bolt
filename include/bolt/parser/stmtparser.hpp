#pragma once

#include <bolt/ast/stmtast.hpp>

namespace bolt
{
    struct LexerIterator;
    class ScopeView;

    namespace detail
    {
        class StmtParser
        {
        public:
            static ast::stmt::IfElseNode ParseIfElse(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::stmt::ForLoopNode ParseForLoop(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::stmt::WhileLoopNode ParseWhileLoop(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::stmt::ReturnNode ParseReturnStmt(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::stmt::BlockNode ParseBlock(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::stmt::ScopeNode ParseScope(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::stmt::VariableDefNode ParseVariableDef(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::stmt::ExprNode ParseTopLevelExpr(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::stmt::Node ParseStmt(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);

        private:
            StmtParser() = delete;
        };
    }
}
