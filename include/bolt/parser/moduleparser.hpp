#pragma once

#include <bolt/ast/moduleast.hpp>

namespace bolt
{
    struct LexerIterator;
    class ScopeView;

    namespace detail
    {
        class ModuleParser
        {
        public:
            static ast::module::Node ParseFunction(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);
            static ast::Module ParseModule(ScopeView& symbolTable, LexerIterator& it, const LexerIterator& end);

        private:
            ModuleParser() = delete;
        };
    }
}
