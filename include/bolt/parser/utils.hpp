#pragma once

#include <bolt/lexer/lexer.hpp>
#include <bolt/parser/exceptions.hpp>

namespace bolt
{
    namespace utils
    {
        namespace parser
        {
            inline void Expect(TokenType tokenType, LexerIterator& it)
            {
                Token currToken = *it;
                if (currToken.type == tokenType)
                {
                    ++it;
                }
                else
                {
                    std::string why("Expected: " + std::to_string(tokenType) + ", got: " + std::to_string(currToken.type) + ".");
                    throw exceptions::SyntaxErrorException(why);
                }
            }

            inline bool IsNextToken(LexerIterator& it, const LexerIterator& end, TokenType token)
            {
                LexerIterator temp = it;
                ++temp;

                return temp == end ? false : (*temp).type == token;
            }

            inline bool IsTypeToken(TokenType token)
            {
                TokenType basicTypes[] = {
                    Bool,
                    Int8, Int16, Int32, Int64,
                    String,
                    UInt8, UInt16, UInt32, UInt64,
                    Void,
                };

                if (std::any_of(std::begin(basicTypes), std::end(basicTypes), [=](TokenType t) { return t == token; }))
                    return true;
                else if (token == Identifier) // User-defined type
                    return true;
                else if (token == Var) // Automatic type deduction
                    return true;
                else
                    return false;
            }
        }
    }
}
