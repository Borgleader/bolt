#pragma once

#include <string>

#include <boost/optional.hpp>

#include <bolt/ast/moduleast.hpp>

namespace bolt
{
    struct LexerIterator;
    class ScopeView;

    class Parser
    {
    public:
        Parser(const std::string& code);
        boost::optional<ast::Module> Parse(SymbolTable& table);

    private:
        Parser() = delete;

        const std::string& code;
    };
}
