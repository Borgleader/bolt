#pragma once

namespace bolt
{
    namespace utils
    {
        template <typename Result>
        Result GetValueOfDigit(char c)
        {
            Result r = 0;
            switch (c)
            {
            case '0': r = 0; break;
            case '1': r = 1; break;
            case '2': r = 2; break;
            case '3': r = 3; break;
            case '4': r = 4; break;
            case '5': r = 5; break;
            case '6': r = 6; break;
            case '7': r = 7; break;
            case '8': r = 8; break;
            case '9': r = 9; break;
            case 'A': r = 10; break;
            case 'B': r = 11; break;
            case 'C': r = 12; break;
            case 'D': r = 13; break;
            case 'E': r = 14; break;
            case 'F': r = 15; break;
            }

            return r;
        }

        template <typename Result, unsigned int Base>
        Result ComputeConstant(std::string input)
        {
            Result total = 0;

            std::size_t numDigits = input.size();
            for (std::size_t i = 0; i < numDigits; ++i)
            {
                total += static_cast<Result>(std::pow(Base, numDigits - i - 1)) * GetValueOfDigit<Result>(input[i]);
            }

            return total;
        }

        template <typename Result>
        Result ParseIntegralConstant(std::string input)
        {
            if (input.size() <= 2)
                return ComputeConstant<Result, 10>(input);

            switch(input[1])
            {
                case 'b': return ComputeConstant<Result,  2>(std::string((input.begin() + 2), input.end()));
                case 'o': return ComputeConstant<Result,  8>(std::string((input.begin() + 2), input.end()));
                case 'd': return ComputeConstant<Result, 10>(std::string((input.begin() + 2), input.end()));
                case 'x': return ComputeConstant<Result, 16>(std::string((input.begin() + 2), input.end()));
                default:  return ComputeConstant<Result, 10>(input);
            }                
        }
    }
}