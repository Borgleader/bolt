#pragma once

namespace bolt
{
    class SymbolTable;

    namespace ast
    {
        struct Module;
    }

    class SemanticAnalyzer
    {
    public:
        SemanticAnalyzer(SymbolTable& table) : symbolTable(table)
        {
        }

        void operator()(ast::Module& module);
        
    private:
        SymbolTable& symbolTable;
    };
}
