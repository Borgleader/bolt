#pragma once

#include <string>
#include <vector>
#include <unordered_map>

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <bolt/symboltable.hpp>

namespace bolt
{
    class SymbolTable
    {
    public:
        struct Function
        {
            std::string returnType;
            std::string name;
            std::vector<std::string> argTypes;
        };

        struct Variable
        {
            std::string type;
            std::string name;
        };

        typedef boost::variant<
            Function,
            Variable
        > Symbol;

        struct Scope
        {
            std::size_t parentIdx;
            std::unordered_map<std::string, Symbol> symbols;
            std::vector<std::size_t> childIdxs;
        };

        SymbolTable()
        {
            // Global scope
            scopes.push_back({ std::size_t(-1), {}, {} });
        }

    private:
        std::size_t Push(std::size_t scopeIdx)
        {
            scopes.push_back({ scopeIdx, {}, {} });

            auto newScopeIdx = scopes.size() - 1;
            scopes[scopeIdx].childIdxs.push_back(newScopeIdx);

            return newScopeIdx;
        }

        boost::optional<Symbol> Lookup(std::size_t scopeIdx, std::string identifier) const
        {
            const Scope* scope = &scopes[scopeIdx];
            
            for (;;)
            {
                auto it = scope->symbols.find(identifier);
                if (it != scope->symbols.end())
                    return it->second;
                else if (scope->parentIdx == std::size_t(-1))
                    return boost::none;

                scope = &scopes[scope->parentIdx];
            }
        }

        boost::optional<Symbol> Find(std::size_t scopeIdx, std::string identifier) const
        {
            auto it = scopes[scopeIdx].symbols.find(identifier);
            if (it != scopes[scopeIdx].symbols.end())
                return it->second;
            else
                return boost::none;
        }

        std::vector<Scope> scopes;

        friend class ScopeView;
    };

    class ScopeView
    {
    public:
        ScopeView(SymbolTable& table) : symbolTable(table), currentScopeIdx(0), currentChildIdx(0)
        {
        }

        void Push()
        {
            currentScopeIdx = symbolTable.Push(currentScopeIdx);
        }

        void Pop()
        {
            currentScopeIdx = symbolTable.scopes[currentScopeIdx].parentIdx;
        }

        boost::optional<SymbolTable::Symbol> Lookup(std::string identifier) const
        {
            return symbolTable.Lookup(currentScopeIdx, identifier);
        }

        boost::optional<SymbolTable::Symbol> Find(std::string identifier) const
        {
            return symbolTable.Find(currentScopeIdx, identifier);
        }
        
        void Add(std::string identifier, SymbolTable::Symbol symbol)
        {
            symbolTable.scopes[currentScopeIdx].symbols[identifier] = symbol;
        }

        ScopeView NextChild()
        {
            ScopeView childView(*this);

            SymbolTable::Scope& currentScope = symbolTable.scopes[currentScopeIdx];
            std::size_t childIdx = currentChildIdx++ % currentScope.childIdxs.size();
            childView.currentScopeIdx = currentScope.childIdxs[childIdx];
            childView.currentChildIdx = 0;

            return childView;
        }

    private:
        SymbolTable& symbolTable;
        std::size_t currentScopeIdx;
        std::size_t currentChildIdx;
    };
}
